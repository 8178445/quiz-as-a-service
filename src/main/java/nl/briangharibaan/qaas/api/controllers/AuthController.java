package nl.briangharibaan.qaas.api.controllers;

import nl.briangharibaan.qaas.persistence.models.JwtToken;
import nl.briangharibaan.qaas.persistence.models.User;
import nl.briangharibaan.qaas.persistence.models.UsernamePasswordToken;
import nl.briangharibaan.qaas.security.TokenHandler;
import nl.briangharibaan.qaas.security.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

@RestController
public class AuthController extends ApiController {

    @Autowired
    private UserService userService;

    @Autowired
    private AuthenticationManager authenticationManager;

    @RequestMapping(value = "/auth", method = RequestMethod.POST)
    @ResponseBody
    public JwtToken getToken(@RequestBody UsernamePasswordToken usernamePasswordToken) throws AuthenticationException {

        User user = userService.loadUserByUsername(usernamePasswordToken.getUsername());

        Authentication auth = new UsernamePasswordAuthenticationToken(user.getUsername(), usernamePasswordToken.getPassword(), user.getAuthorities());

        auth = authenticationManager.authenticate(auth);
        SecurityContextHolder.getContext().setAuthentication(auth);

        TokenHandler tokenHandler = new TokenHandler("tooManySecrets", userService);
        String token = tokenHandler.createTokenForUser(user);

        JwtToken jwtToken = new JwtToken(token);

        return jwtToken;
    }
}
