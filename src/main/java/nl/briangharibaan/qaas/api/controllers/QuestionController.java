package nl.briangharibaan.qaas.api.controllers;

import nl.briangharibaan.qaas.persistence.models.Answer;
import nl.briangharibaan.qaas.persistence.models.Question;
import nl.briangharibaan.qaas.persistence.repositories.QuestionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class QuestionController extends ApiController {

    @Autowired
    QuestionRepository questionRepository;

    @RequestMapping(value = "/questions", method = RequestMethod.GET)
    public List<Question> showAll() {
        return (List<Question>) questionRepository.findAll();
    }

    @RequestMapping(value = "/questions/{questionId}", method = RequestMethod.GET)
    public Question show(@PathVariable("questionId") long questionId) {
        return questionRepository.findOne(questionId);
    }

    @Secured("ROLE_ADMIN")
    @RequestMapping(value = "/questions", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<?> save(@RequestBody Question question) throws Exception {
        if (question.getId() > 0) {
            return new ResponseEntity<String>(HttpStatus.BAD_REQUEST);
        }

        questionRepository.save(question);
        return new ResponseEntity(question, HttpStatus.OK);
    }

    @Secured("ROLE_ADMIN")
    @RequestMapping(value = "/questions/{questionId}", method = RequestMethod.PUT)
    @ResponseBody
    public ResponseEntity<?> update(@RequestBody Question question, @PathVariable("questionId") long questionId) {
        if (questionId != question.getId()) {
            return new ResponseEntity<String>(HttpStatus.BAD_REQUEST);
        }

        for (Answer answer : question.getAnswers()) {
            answer.setQuestion(question);
        }

        questionRepository.save(question);
        return new ResponseEntity(question, HttpStatus.OK);
    }


}
