package nl.briangharibaan.qaas.persistence.repositories;

import nl.briangharibaan.qaas.persistence.models.Question;
import org.springframework.data.repository.CrudRepository;

public interface QuestionRepository extends CrudRepository<Question, Long> {
}
