package nl.briangharibaan.qaas.persistence.repositories;

import nl.briangharibaan.qaas.persistence.models.User;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User, Long> {
    public User findByUsername(String username);
}
