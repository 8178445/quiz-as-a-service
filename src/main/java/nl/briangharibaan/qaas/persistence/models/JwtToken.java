package nl.briangharibaan.qaas.persistence.models;

public class JwtToken {

    private final String token;

    public JwtToken(String token) {
        this.token = token;
    }

    public String getToken() {
        return token;
    }
}
