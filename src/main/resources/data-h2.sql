-- Populate questions
INSERT INTO question (id, question) VALUES (1, 'How tall is the eiffel tower?');
INSERT INTO answer (id, answer, is_correct, question_id) VALUES
  (1, '300 metres', true, 1),
  (2, '400 metres', false, 1),
  (3, '800 metres', false, 1)
;
INSERT INTO question (id, question) VALUES (2, 'How many wheels does a car have?');
INSERT INTO answer (id, answer, is_correct, question_id) VALUES
  (4, 'sixteen wheels', false, 2),
  (5, 'two wheels', false, 2),
  (6, 'four wheels', true, 2)
;

-- Populate users
INSERT INTO user(id, username,password) VALUES (1, 'brian@jongensvantechniek.nl','$2a$10$KDxiOd1vc8imTikUp/0LVec.A4wZ.o9XpHkMcI7yoyd4TQvxcvsL6');
INSERT INTO role(id, role) VALUES (1, 'ROLE_USER');
INSERT INTO user_roles(users_id, roles_id) VALUES (1,1);
INSERT INTO user(id, username,password) VALUES (2, 'admin@jongensvantechniek.nl','$2a$10$KDxiOd1vc8imTikUp/0LVec.A4wZ.o9XpHkMcI7yoyd4TQvxcvsL6');
INSERT INTO role(id, role) VALUES (2, 'ROLE_ADMIN');
INSERT INTO user_roles(users_id, roles_id) VALUES
  (2,1),
  (2,2)
;